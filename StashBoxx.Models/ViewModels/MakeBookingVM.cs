﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StashBoxx.Models.ViewModels
{
    public class MakeBookingVM
    {
        public DateTime StartTime { get; set; }
        public int DurationHours { get; set; }
    }
}

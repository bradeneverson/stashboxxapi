﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StashBoxx.Models.DataModels
{
    public class Booking
    {
        public int LockerNum { get; set; }
        public string AccessCode { get; set; }
        public DateTime StartingTime { get; set; }
        public DateTime EndingTime { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StashBoxx.Models.DataModels
{
    public class JsonBooking
    {
      public List<JsonBookingItem> AllBookings { get; set; }
    }
    public class JsonBookingItem
    {
        public string AccessCode { get; set; }
        public int LockerNum { get; set; }
        public string StartingTime { get; set; }
        public string EndingTime { get; set; }
    }
}

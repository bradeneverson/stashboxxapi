﻿namespace StashBoxx.Services
{
    public class JsonBooking
    {
        public List<JsonBookingItem> AllBookings { get; set; }
    }
    public class JsonBookingItem
    {
        public string AccessCode { get; set; }
        public int LockerNum { get; set; }
        public string StartingTime { get; set; }
        public string EndingTime { get; set; }
    }
}
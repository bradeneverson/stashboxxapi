﻿using StashBoxx.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StashBoxx.Services.Interfaces
{
    public interface IBookingService
    {
        public bool AddBooking(Booking booking);
        public int UnlockLocker(string AccessCode);
    }
}

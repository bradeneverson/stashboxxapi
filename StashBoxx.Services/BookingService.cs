﻿using Newtonsoft.Json;
using StashBoxx.Models.DataModels;
using StashBoxx.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StashBoxx.Services
{
    public class BookingService : IBookingService
    {
        public bool AddBooking(Booking booking)
        {
            bool TimeSlotOpen = true;
            List<Booking> bookings = GetBookings();
            foreach(Booking testBooking in bookings.Where(b => b.LockerNum == booking.LockerNum).ToList())
            {
                if(!((booking.StartingTime >= testBooking.StartingTime && booking.StartingTime < testBooking.EndingTime) || (booking.EndingTime > testBooking.StartingTime && booking.EndingTime <= testBooking.EndingTime)))
                {
                    //Times do not conflict
                    TimeSlotOpen = true;
                }
                else
                {
                    //Times do conflict
                    TimeSlotOpen = false;
                    break;
                }
            }
            if (TimeSlotOpen)
            {
                bookings.Add(booking);
                JsonBooking jsonBooking = new JsonBooking();
                jsonBooking.AllBookings = new List<JsonBookingItem>();
                foreach(Booking addedBooking in bookings)
                {
                    JsonBookingItem serializedBookingItem = new JsonBookingItem()
                    {
                        AccessCode = addedBooking.AccessCode,
                        LockerNum = addedBooking.LockerNum,
                        StartingTime = addedBooking.StartingTime.ToString(),
                        EndingTime = addedBooking.EndingTime.ToString()
                    };
                    jsonBooking.AllBookings.Add(serializedBookingItem);
                }
                File.WriteAllText(@"DevData/Bookings.json", JsonConvert.SerializeObject(jsonBooking));
                return true;
            }
            return false;

        }

        public int UnlockLocker(string AccessCode)
        {
            var time = DateTime.Now.ToLocalTime();
            TimeZoneInfo edtZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");

            time = TimeZoneInfo.ConvertTime(time, edtZone).AddHours(1);//For Florida
            Booking booking = GetBookings().FirstOrDefault(b => b.AccessCode == AccessCode);
            if(time >= booking.StartingTime && time <= booking.EndingTime)
            {
                return booking.LockerNum;
            }
            else
            {
                return -1;
            }
        }

        internal List<Booking> GetBookings()
        {
            List<Booking> bookings = new List<Booking>();
            JsonBooking jsonBooking = JsonConvert.DeserializeObject<JsonBooking>(System.IO.File.ReadAllText("DevData/Bookings.json"));

            foreach(JsonBookingItem bookingItem in jsonBooking.AllBookings)
            {
                Booking booking = new Booking()
                {
                    AccessCode = bookingItem.AccessCode,
                    LockerNum = bookingItem.LockerNum,
                    StartingTime = DateTime.Parse(bookingItem.StartingTime),
                    EndingTime =  DateTime.Parse(bookingItem.EndingTime)
                };
                bookings.Add(booking);
            }

            return bookings;
        }

    }
}

using StashBoxx.Services;
using StashBoxx.Services.Interfaces;

namespace StashBoxxAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddControllersWithViews();
            builder.Services.AddScoped<IBookingService, BookingService>();

            #region HerokuLaunchSettings
            //Heroku Launch Protocal, comment out if testing locally
            var port = Environment.GetEnvironmentVariable("PORT");
            builder.WebHost.ConfigureKestrel(options =>
            {
                options.ListenAnyIP(Int32.Parse(port));
            });
            #endregion
            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");

            app.Run();
        }
    }
}
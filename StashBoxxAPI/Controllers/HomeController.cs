﻿using Microsoft.AspNetCore.Mvc;
using StashBoxx.Models.ViewModels;
using StashBoxx.Models.DataModels;
using StashBoxxAPI.Models;
using System.Diagnostics;
using StashBoxx.Services.Interfaces;

namespace StashBoxxAPI.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IBookingService _bookingService;
        public HomeController(ILogger<HomeController> logger, IBookingService bookingService)
        {
            _logger = logger;
            _bookingService = bookingService;
        }

        public IActionResult Index()
        {
            //Create form for testing bookings

            return View();
        }
        [HttpPost]
        public IActionResult Index(MakeBookingVM bookingVM)
        {
            Booking booking = new Booking()
            {
                AccessCode = Guid.NewGuid().ToString(),
                StartingTime = bookingVM.StartTime,
                EndingTime = bookingVM.StartTime.AddHours(bookingVM.DurationHours),
                LockerNum = 1 //Temp test for all lockers
            };
            bool success = _bookingService.AddBooking(booking);
            TempData["Success"] = success;
            if (success)
            {
                TempData["Message"] = "Your Booking Was Successfully Uploaded with Access Code: " + booking.AccessCode;
            }
            else
            {
                TempData["Message"] = "Your Booking Time Overlaps with An Already Scheduled Time, Please Try A Different Time";
            }
            return RedirectToAction("Index");
        }

        public JsonResult CheckLockerAvailability(string AccessCode)
        {
            return new JsonResult(_bookingService.UnlockLocker(AccessCode));
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}